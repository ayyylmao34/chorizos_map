<?php

use Phinx\Migration\AbstractMigration;

class CreateTables extends AbstractMigration
{
    public function up() {

        // Creacion de la tabla de users
        $users_table = $this->table('users');
        $users_table->addColumn('email', 'string', ['limit' => 255])
            ->addColumn('password', 'string', ['limit' => 40])
            ->addColumn('name', 'string', ['default' => ''])
            ->addColumn('lastname', 'string', ['default' => ''])
            ->addColumn('birthdate', 'date', ['default' => ''])
            ->addColumn('is_admin', 'boolean', ['default' => false])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->save();

        // Insert a la tabla de usuarios
        $users_table->insert([
            [
                'email'    => 'admin@gmail.com',
                'password' => hash("sha1", 'azsxdc'),
                'is_admin' => true,
            ],
            [
                'email'    => 'user1@gmail.com',
                'password' => hash("sha1", 'azsxdc'),
                'is_admin' => false,
            ],
            [
                'email'    => 'user2@gmail.com',
                'password' => hash("sha1", 'azsxdc'),
                'is_admin' => false,
            ],
            [
                'email'    => 'user3@gmail.com',
                'password' => hash("sha1", 'azsxdc'),
                'is_admin' => false,
            ],
            [
                'email'    => 'user4@gmail.com',
                'password' => hash("sha1", 'azsxdc'),
                'is_admin' => false,
            ],
        ]);
        $users_table->saveData();

        // Creacion de la tabla crime_types
        $crime_types_table = $this->table('crime_types');
        $crime_types_table->addColumn('name', 'string', ['limit' => 30])
            ->addColumn('icon', 'string', ['limit' => 7])
            ->save();

        // Insert de tipos de crimenes
        $crime_types_table->insert([
            ['name' => 'Asalto',    'icon' => 'asa.png'],
            ['name' => 'Asesinato', 'icon' => 'ase.png'],
            ['name' => 'Carterazo', 'icon' => 'car.png'],
            ['name' => 'Robo',      'icon' => 'rob.png'],
        ]);
        $crime_types_table->saveData();

        // Creacion de la tabla crimes
        $crimes_table = $this->table('crimes');
        $crimes_table->addColumn('user_id', 'integer')
            ->addForeignKey('user_id', 'users', 'id')
            ->addColumn('crime_type_id', 'integer')
            ->addForeignKey('crime_type_id', 'crime_types', 'id')
            ->addColumn('title', 'string', ['limit' => 50])
            ->addColumn('date', 'date')
            ->addColumn('description', 'string', ['limit' => 255])
            ->addColumn('lat', 'float')
            ->addColumn('lng', 'float')
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->save();

        // Insert de crimenes
        $crimes_table->insert([
            [
                'user_id'       => 2,
                'crime_type_id' => 1,
                'title'         => 'Me asaltaron brigido',
                'date'          => '2015-05-01',
                'description'   => 'Unos logis me robaron mi iphone 6s que justo se lo habia robado a un perkinazo',
                'lat'           => -36.6057472,
                'lng'           => -72.098007,
            ],
            [
                'user_id'       => 3,
                'crime_type_id' => 2,
                'title'         => 'Asesinaron al bráyátán',
                'date'          => '2015-05-2',
                'description'   => 'Se lo pitiaron unos jiles de la frontera',
                'lat'           => -36.6122855,
                'lng'           => -72.1029116,
            ],
            [
                'user_id'       => 4,
                'crime_type_id' => 3,
                'title'         => 'Me robaron la cartera unos flaytes',
                'date'          => '2015-05-3',
                'description'   => 'No puede ser, nooo mi preciada cartera se ha ido con esos maleantes! que alguien los atrape!!!!',
                'lat'           => -36.6158239,
                'lng'           => -72.097102,
            ],
            [
                'user_id'       => 5,
                'crime_type_id' => 4,
                'title'         => 'Me robaron mi local',
                'date'          => '2015-05-4',
                'description'   => 'Una banda de 5 criminales se metio a mi local y se robaron todo los csm!',
                'lat'           => -36.6027472,
                'lng'           => -72.098007,
            ],
        ]);
        $crimes_table->saveData();

        // Creacion de la tabla contacts
        $contact_table = $this->table('contacts');
        $contact_table->addColumn('name', 'string', ['limit' => 30])
            ->addColumn('email', 'string', ['limit' => 255])
            ->addColumn('phone', 'string', ['limit' => 30])
            ->addColumn('message', 'string', ['limit' => 255])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->save();
    }

    public function down() {
        $this->dropTable('contacts');
        $this->dropTable('crimes');
        $this->dropTable('crime_types');
        $this->dropTable('users');
    }
}
