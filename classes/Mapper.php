<?php

abstract class Mapper {
    protected $db;

    /**
     * Constructor
     *
     * @param PDO $db
     */
    public function __construct($db) {
        $this->db = $db;
    }

}
