<?php

class CrimeTypeMapper extends Mapper
{
    /**
     * Devuelve los tipos de crimenes
     *
     * @return array $results
     */
    public function getAjaxCrimeTypes() {
        $sql = "SELECT id,name,icon FROM crime_types";

        $stmt = $this->db->query($sql);

        $results = [];

        while ($row = $stmt->fetch()) {
            $results[] = [
                'id'   => $row['id'],
                'name' => $row['name'],
                'icon' => $row['icon'],
            ];
        }

        return $results;
    }
}