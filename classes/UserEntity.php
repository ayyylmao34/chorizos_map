<?php

class UserEntity
{
    public $id;
    public $username;
    public $name;
    public $lastname;
    public $birtdate;
    public $email;
    public $password;
    public $password_confirmation;
    public $is_admin;
    public $created_at;

    /**
     * Accept an array of data matching properties of this class
     * and create the class
     *
     * @param array $data The data to use to create
     */
    public function __construct(array $data)
    {
        $this->id                    = isset($data['id']) ? $data['id'] : null;
        $this->email                 = isset($data['email']) ? $data['email'] : null;
        $this->username              = isset($data['username']) ? $data['username'] : null;
        $this->name                  = isset($data['name']) ? $data['name'] : null;
        $this->lastname              = isset($data['lastname']) ? $data['lastname'] : null;
        $this->birthdate             = isset($data['birthdate']) ? $data['birthdate'] : null;
        $this->password              = isset($data['password']) ? $data['password'] : null;
        $this->password_confirmation = isset($data['password_confirmation']) ? $data['password_confirmation'] : null;
        $this->is_admin              = isset($data['is_admin']) ? $data['is_admin'] : null;
        $this->created_at            = isset($data['created_at']) ? $data['created_at'] : null;
    }

    /**
     * Valida el formulario de creacion/actualizacion del crimen
     *
     * @param array $data
     * @return array $errors
     */
    public static function validate($data)
    {
        $errors = [];

        if (empty($data['email'])) {
            $errors[] = 'Ingrese su email';
        }

        if (empty($data['username'])) {
            $errors[] = 'Ingrese su nombre de usuario';
        }

        if (empty($data['name'])) {
            $errors[] = 'Ingrese su nombre';
        }

        if (empty($data['lastname'])) {
            $errors[] = 'Ingrese su apellido';
        }

        if (empty($data['birthdate'])) {
            $errors[] = 'Ingrese su fecha de nacimiento';
            return $errors;
        }

        error_log($data['birthdate']);

        if (preg_match('/\d{4}-\d{2}-\d{2}/', $data['birthdate']) == 0) {
            $errors[] = 'Ingrese una fecha valida';
        }

        if (empty($data['password'])) {
            $errors[] = 'Ingrese su clave';
            return $errors;
        }

        if (empty($data['password_confirmation'])) {
            $errors[] = 'Ingrese la confirmacion de su clave';
            return $errors;
        }

        if (strlen($data['password']) < 5) {
            $errors[] = 'Su clave debe tener almenos 5 caracteres';
            return $errors;
        }

        if ($data['password'] != $data['password_confirmation']) {
            $errors[] = 'Las claves no son iguales';
        }

        return $errors;
    }
}