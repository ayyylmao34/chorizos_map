<?php

class UserMapper extends Mapper
{
    /**
     * Valida si existe el usuario segun el email y la clave
     *
     * @param string $email
     * @param string $password
     * @return array $user
     */
    public function login($email = "", $password = "") {
        $sql = "SELECT id,is_admin,username FROM users
            WHERE email = :email AND password = :password";

        $stmt = $this->db->prepare($sql);
        $stmt->execute([
            'email'    => $email,
            'password' => $password
        ]);

        $user['exists'] = false;

        if ($row = $stmt->fetch()) {
            $user['exists']   = true;
            $user['username'] = $row['username'];
            $user['id']       = $row['id'];
            $user['admin']    = $row['is_admin'];
        }

        return $user;
    }

    /**
     * Devuelve todos los usuarios
     *
     * @return array $results
     */
    public function getUsers() {
        $sql = "SELECT id,email,password,created_at FROM users";

        $stmt = $this->db->query($sql);

        $results = [];

        while ($row = $stmt->fetch()) {
            $results[] = new UserEntity($row);
        }

        return $results;
    }

    /**
     * Devuelve un usuario segun el id
     *
     * @param int $id
     * @return UserEntity
     */
    public function getUserById($id) {
        $sql = "SELECT id,email,username,password,created_at FROM users
            WHERE id = :user_id";

        $stmt = $this->db->prepare($sql);
        $stmt->execute(['user_id' => $id]);

        return new UserEntity($stmt->fetch());
    }

    /**
     * Crea un nuevo usuario
     *
     * @param array $user
     * @param boolean $is_admin
     * @return array $user | int
     */
    public function create($user, $is_admin = false) {
        $sql = "INSERT INTO users(email,username,name,lastname,birthdate,password,is_admin)
                VALUES(:email,:username,:name,:lastname,:birthdate,:password,:is_admin)";

        $stmt = $this->db->prepare($sql);

        try {
            $this->db->beginTransaction();

            $stmt->execute([
                'email'     => $user['email'],
                'username'  => $user['username'],
                'name'      => $user['name'],
                'lastname'  => $user['lastname'],
                'birthdate' => $user['birthdate'],
                'password'  => hash("sha1", $user['password']),
                'is_admin'  => $is_admin
            ]);

            $this->db->commit();

            $id = $this->db->lastInsertId();

            return new UserEntity([
                'id'       => $id,
                'username' => $user['username'],
                'name'     => $user['name'],
                'email'    => $user['email'],
                'password' => $user['password'],
                'is_admin' => $is_admin,
            ]);

        } catch (Exception $e) {
            $this->db->rollback();

            if ($e->getCode() == 23000) {
                return -9;
            }

            error_log("Error! - (UserMapper@create) - " . $e->getMessage());
            return 0;
        }
    }
}