<?php

class CrimeEntity
{
    public $id;
    public $user_id;
    public $crime_type_id;
    public $title;
    public $date;
    public $description;
    public $lat;
    public $lng;
    public $created_at;

    /**
     * Accept an array of data matching properties of this class
     * and create the class
     *
     * @param array $data The data to use to create
     */
    public function __construct(array $data)
    {
        $this->id            = isset($data['id']) ? $data['id'] : null;
        $this->user_id       = isset($data['user_id']) ? $data['user_id'] : null;
        $this->crime_type_id = isset($data['crime_type_id']) ? $data['crime_type_id'] : null;
        $this->title         = isset($data['title']) ? $data['title'] : null;
        $this->date          = isset($data['date']) ? $data['date'] : null;
        $this->description   = isset($data['description']) ? $data['description'] : null;
        $this->lat           = isset($data['lat']) ? $data['lat'] : null;
        $this->lng           = isset($data['lng']) ? $data['lng'] : null;
        $this->created_at    = isset($data['created_at']) ? $data['created_at'] : null;
    }

    /**
     * Valida el formulario de creacion/actualizacion del crimen
     *
     * @param array $data
     * @return array $errors
     */
    public static function validate($data) {
        $errors = [];

        if (empty($data['lat']) || empty($data['lng'])) {
            $errors[] = 'Seleccione un punto';
        }

        if (empty($data['title'])) {
            $errors[] = 'Ingrese un titulo';
        }

        if (empty($data['date'])) {
            $errors[] = 'Ingrese una fecha';
        }

        if (empty($data['description'])) {
            $errors[] = 'Ingrese una descripcion';
        }

        return $errors;
    }
}