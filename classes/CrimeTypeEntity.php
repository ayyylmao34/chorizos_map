<?php

class CrimeTypeEntity
{
    public $id;
    public $name;
    public $icon;

    /**
     * Accept an array of data matching properties of this class
     * and create the class
     *
     * @param array $data The data to use to create
     */
    public function __construct(array $data)
    {
        $this->id   = isset($data['id']) ? $data['id'] : null;
        $this->name = isset($data['name']) ? $data['name'] : null;
        $this->icon = isset($data['icon']) ? $data['icon'] : null;
    }

}