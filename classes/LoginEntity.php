<?php

class LoginEntity
{
    /**
     * Valida el formulario de inicio de sesion
     *
     * @param array $data
     * @return array $errors
     */
    public static function validate($data) {
        $errors = [];

        if (empty($data['email'])) {
            $errors[] = 'Ingrese su email';
        }

        if (empty($data['password'])) {
            $errors[] = 'Ingrese su clave';
        }

        return $errors;
    }
}