<?php

class CrimeMapper extends Mapper
{
    /**
     * Devuelve los ultimos 20 crimenes
     *
     * @return array $results
     */
    public function getAjaxCrimes() {
        $sql = "SELECT crimes.id,lat,lng,title,description,user_id,DATE_FORMAT(date, '%d-%m-%Y') as date,crime_types.icon
            FROM crimes JOIN crime_types
            ON crimes.crime_type_id = crime_types.id
            ORDER BY crimes.id DESC
            LIMIT 20";

        $stmt = $this->db->query($sql);

        $results = [];

        while ($row = $stmt->fetch()) {
            $results[] = [
                'id'          => $row['id'],
                'lat'         => $row['lat'],
                'lng'         => $row['lng'],
                'title'       => $row['title'],
                'description' => $row['description'],
                'user_id'     => $row['user_id'],
                'icon'        => $row['icon'],
                'date'        => $row['date'],
            ];
        }

        return $results;
    }

    /**
     * Crea un nuevo crimen
     *
     * @param array $crime
     * @return CrimeEntity | int
     */
    public function create($crime = []) {
        $sql = "INSERT INTO crimes(user_id,crime_type_id,title,date,description,lat,lng)
            VALUES(:user_id,:crime_type_id,:title,:date,:description,:lat,:lng)";

        $stmt = $this->db->prepare($sql);

        try {
            $this->db->beginTransaction();

            $stmt->execute([
                'user_id'       => $crime['user_id'],
                'crime_type_id' => $crime['crime_type_id'],
                'title'         => $crime['title'],
                'date'          => $crime['date'],
                'description'   => $crime['description'],
                'lat'           => $crime['lat'],
                'lng'           => $crime['lng'],
            ]);

            $this->db->commit();

            $id = $this->db->lastInsertId();

            return new CrimeEntity([
                'id'            => $id,
                'user_id'       => $crime['user_id'],
                'crime_type_id' => $crime['crime_type_id'],
                'title'         => $crime['title'],
                'date'          => $crime['date'],
                'description'   => $crime['description'],
                'lat'           => $crime['lat'],
                'lng'           => $crime['lng'],
            ]);

        } catch (Exception $e) {
            $this->db->rollback();
            error_log("Error! - (CrimeMapper@create) - " . $e->getMessage());
            return 0;
        }
    }

    /**
     * Devuelve un crimen segun el id
     *
     * @param int $id
     * @throws Exception $e
     * @return CrimeEntity
     */
    public function getCrimeById($id) {
        $sql = "SELECT id,user_id,crime_type_id,title,date,description,lat,lng,created_at
            FROM crimes
            WHERE id = :crime_id";

        $stmt = $this->db->prepare($sql);
        $stmt->execute(['crime_id' => $id]);

        $result = $stmt->fetch();

        if ($result == false) {
            throw new Exception("Error not found", 1);
        }

        return new CrimeEntity($result);
    }

    /**
     * Actualiza un crimen
     *
     * @param array $crime
     * @return CrimeEntity | int
     */
    public function update($crime = []) {
        $sql = "UPDATE crimes SET crime_type_id = :crime_type_id, title = :title,
                date = :date, description = :description, lat = :lat, lng = :lng
                WHERE id = :id";

        $stmt = $this->db->prepare($sql);

        try {
            $this->db->beginTransaction();
            $stmt->execute([
                'crime_type_id' => $crime['crime_type_id'],
                'title'         => $crime['title'],
                'date'          => $crime['date'],
                'description'   => $crime['description'],
                'lat'           => $crime['lat'],
                'lng'           => $crime['lng'],
                'id'            => $crime['id'],
            ]);

            $this->db->commit();
            return $crime;

        } catch (Exception $e) {
            $this->db->rollback();
            error_log("Error! - (CrimeMapper@update) - " . $e->getMessage());
            return 0;
        }
    }

    /**
     * Elimina un crimen segun el id
     *
     * @param int $id
     * @return int
     */
    public function delete($id) {
        $sql = "DELETE FROM crimes WHERE id = :id";

        $stmt = $this->db->prepare($sql);

        try {
            $this->db->beginTransaction();

            $stmt->execute(['id' => $id]);

            $this->db->commit();

            return $stmt->rowCount();

        } catch (Exception $e) {
            $this->db->rollback();
            error_log("Error! - (CrimeMapper@delete) - " . $e->getMessage());
            return -1;
        }
    }
}