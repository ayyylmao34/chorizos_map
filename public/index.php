<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Se cargan los componentes de composer
require '../vendor/autoload.php';

// Se cargan las clases del modelo de datos
spl_autoload_register(function ($classname) {
    require("../classes/" . $classname . ".php");
});

// Configuraciones globales (Base de datos)
$config = [
    'displayErrorDetails' => true,
    'db' => [
        'host'   => 'localhost',
        'user'   => 'root',
        'pass'   => 'azsxdc',
        'dbname' => 'mapkronol'
    ]
];

// Se inicia una sesion
session_cache_limiter(false);
session_start();

// Se crea la aplicacion (Slim framework)
$app = new \Slim\App(['settings' => $config]);
$container = $app->getContainer();

// Verifica si el usuario inicio sesion
$isLogged = function ($req, $res, $next) {
    if (!$_SESSION['email']) {
        return $res->withRedirect("/login");
    }

    return $next($req, $res);
};

// Verifica si el usuario es Administrador
$isAdmin = function ($req, $res, $next) {
    if (!$_SESSION['admin'] || !$_SESSION['email']) {
        return $res->withRedirect("/");
    }

    return $next($req, $res);
};

// Se configura el logger
$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    $logger->pushHandler($file_handler);

    return $logger;
};

// Se configura el motor de plantillas Twig
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../templates/', ['cache' => false]);
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));
    $view->getEnvironment()->addGlobal("session", $_SESSION);

    return $view;
};

// Se configura la base de datos
$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pass']);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    return $pdo;
};

// Muestra la pagina de inicio
$app->get('/', function (Request $req, Response $res) {
    return $this->view->render($res, "home.html");
});

// Muestra el formulario para ingresar un crimen
$app->get('/nuevo-crimen', function (Request $req, Response $res) {
    return $this->view->render($res, "new-crime.html", [
        'errors' => [],
        'edit'   => 0,
        'crime'  => new CrimeEntity([
            'lat'           => -36.6079547,
            'lng'           => -72.1044419,
            'crime_type_id' => 1,
            'date'          => date('Y-m-d'),
            'description'   => '',
            'title'         => ''
        ])
    ]);
})->add($isLogged);

// Guarda el nuevo crimen
$app->post('/nuevo-crimen', function (Request $req, Response $res) {
    $data = $req->getParsedBody();
    $data['user_id'] = $_SESSION['user_id'];

    $errors = CrimeEntity::validate($data);

    if (!empty($errors)) {
        return $this->view->render($res, "new-crime.html", [
            'errors' => $errors,
            'crime'  => new CrimeEntity($data),
            'edit'   => 1,
        ]);
    }

    $crime = (new CrimeMapper($this->db))->create($data);

    if ($crime) {
        $_SESSION['success'] = 'Crimen guardado exitosamente';
        return $res->withRedirect("/#map");
    }

    $errors[] = "Error de conexion interno al guardar el crimen, Contacte con un administrador.";

    return $this->view->render($res, "new-crime.html", ['errors' => $errors]);

})->add($isLogged);

// Muestra el formulario para iniciar sesion
$app->get('/login', function (Request $req, Response $res) {
    if ($_SESSION['email']) {
        return $res->withRedirect("/");
    }

    return $this->view->render($res, "login.html", ['errors' => []]);
});

// Valida email y passsword e inicia sesion
$app->post('/login', function (Request $req, Response $res) use ($app) {
    $data = $req->getParsedBody();

    $errors = LoginEntity::validate($data);

    if (!empty($errors)) {
        return $this->view->render($res, "login.html", ['errors' => $errors]);
    }

    $user = (new UserMapper($this->db))->login($data['email'], hash("sha1", $data['password']));

    if ($user['exists']) {
        $_SESSION['email']    = $data['email'];
        $_SESSION['user_id']  = $user['id'];
        $_SESSION['username'] = $user['username'];
        $_SESSION['admin']    = $user['admin'];
        return $res->withRedirect("/");
    }

    return $this->view->render($res, "login.html", ['errors' => ['El usuario no existe']]);
});

// Cerrar sesion y redirecciona al inicio
$app->get('/logout', function (Request $req, Response $res) {
    session_destroy();
    return $res->withRedirect("/");
});

// Muestra un crimen en especifico
$app->get('/crimen/{id}', function (Request $req, Response $res, $args) {
    var_dump($args['id']);
});

// Devuelve un JSON con los ultimos crimenes
$app->get('/api/crimes', function (Request $req, Response $res) {
    $crime_mapper = new CrimeMapper($this->db);
    $crimes = $crime_mapper->getAjaxCrimes();

    return $res->withJson($crimes);
});

// Devuelve un JSON con los tipos de crimenes
$app->get('/api/crime-types', function (Request $req, Response $res) {
    $crime_type_mapper = new CrimeTypeMapper($this->db);
    $crime_types = $crime_type_mapper->getAjaxCrimeTypes();

    return $res->withJson($crime_types);
});

// Muestra todos los crimenes
$app->get('/admin/crimenes', function (Request $req, Response $res) {
    return $this->view->render($res, "admin/crimenes.html");
});

// Elimina un crimen segun su id
$app->get('/crimen/{id}/editar', function (Request $req, Response $res, $args) {
    $crime_mapper = new CrimeMapper($this->db);

    try {
        $crime = $crime_mapper->getCrimeById($args['id']);
    } catch (Exception $e) {
        return $res->withRedirect("/");
    }

    if ($_SESSION['admin'] || $crime->user_id == $_SESSION['user_id']) {
        return $this->view->render($res, "edit-crime.html", [
            'errors' => [],
            'edit'   => 1,
            'crime'  => $crime
        ]);
    }

    return $res->withRedirect("/");

})->add($isLogged);

// Elimina un crimen segun su id
$app->post('/crimen/{id}/editar', function (Request $req, Response $res, $args) {
    $crime_mapper = new CrimeMapper($this->db);

    try {
        $crime = $crime_mapper->getCrimeById($args['id']);
    } catch (Exception $e) {
        return $res->withRedirect("/");
    }

    if ($crime->user_id != $_SESSION['user_id'] && !$_SESSION['admin']) {
        return $res->withRedirect("/");
    }

    $data = $req->getParsedBody();
    $data['id'] = $crime->id;

    $errors = CrimeEntity::validate($data);

    if (!empty($errors)) {
        return $this->view->render($res, "edit-crime.html", [
            'errors' => $errors,
            'crime'  => $data,
            'edit'   => 1,
        ]);
    }

    $crime = (new CrimeMapper($this->db))->update($data);

    if ($crime) {
        $_SESSION['success'] = 'Crimen actualizado exitosamente';
        return $res->withRedirect("/#map");
    }

})->add($isLogged);

// Elimina un crimen segun su id
$app->get('/crimen/{id}/eliminar', function (Request $req, Response $res, $args) {
    $crime_mapper = new CrimeMapper($this->db);

    try {
        $crime = $crime_mapper->getCrimeById($args['id']);
    } catch (Exception $e) {
        return $res->withRedirect("/");
    }

    if ($crime->user_id != $_SESSION['user_id']) {
        return $res->withRedirect("/");
    }

    $result = $crime_mapper->delete($args['id']);

    if ($result == 1) {
        $_SESSION['info'] = 'Crimen eliminado exitosamente';
    }

    if ($result == 0) {
        $_SESSION['info'] = 'El crimen no existe';
    }

    if ($result == -1) {
        $_SESSION['info'] = 'Error al eliminar el crimen :(';
    }

    return $res->withRedirect("/#map");

})->add($isLogged);

// Muestra el formulario de registro
$app->get('/registro', function (Request $req, Response $res) {
    return $this->view->render($res, "registro.html", [
        'errors' => [],
        'user'   => new UserEntity([])
    ]);
});

// Guarda el formulario de registro
$app->post('/registro', function (Request $req, Response $res) {
    $data = $req->getParsedBody();

    $errors = UserEntity::validate($data);

    if (!empty($errors)) {
        return $this->view->render($res, "registro.html", [
            'errors' => $errors,
            'user'   => new UserEntity($data),
        ]);
    }

    $user = (new UserMapper($this->db))->create($data, false);

    if ($user == -9) {
        return $this->view->render($res, "registro.html", [
            'errors' => ['El usuario ya existe'],
            'user'   => new UserEntity($data),
        ]);
    }

    if ($user) {
        return $res->withRedirect("/login");
    }

    $errors[] = "Error de conexion interno al guardar el crimen, Contacte con un administrador.";

    return $this->view->render($res, "registro.html", ['errors' => $errors]);
});

// Se ejecuta la aplicacion
$app->run();